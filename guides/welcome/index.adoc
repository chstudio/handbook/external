= Welcome Guide
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2021-12
:lang: fr-FR

> Bienvenue dans l'équipe !

Ce dépôt a pour objectif de servir de base de connaissance et de guide
d'intégration. Les outils utilisés et les pratiques de développements y sont
détaillées.

== Méthode

* xref:methods/dev-workflow.adoc[Workflow de développement]

== Outils

* xref:tools/gpg.adoc[OpenPGP et clés GPG]
* xref:tools/yubikey.adoc[YubiKeys]
* xref:tools/bitwarden.adoc[Bitwarden]
* xref:tools/excalidraw.adoc[ExcaliDraw]

== Boite à outils développement

* xref:tools/git.adoc[Git]
* xref:tools/gitlab.adoc[Gitlab]
* xref:tools/ssh.adoc[SSH]
* xref:tools/docker.adoc[Docker]
* xref:tools/makefile.adoc[`make` && Makefile]
* xref:tools/asciidoctor.adoc[AsciiDoctor]
* xref:tools/phpunit.adoc[PHPUnit]
* xref:tools/phpspec.adoc[PHPSpec]

== Languages

* xref:languages/php.adoc[PHP]
* xref:languages/sql.adoc[SQL]
* xref:languages/markdown.adoc[Markdown]
* xref:languages/asciidoc.adoc[AsciiDoc]
