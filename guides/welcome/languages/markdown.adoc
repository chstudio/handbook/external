= link:https://daringfireball.net/projects/markdown/[Markdown]
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2022-10
:lang: fr-FR

== Resources

* https://daringfireball.net/projects/markdown/
* https://fr.wikipedia.org/wiki/Markdown
* https://github.github.com/gfm/
* https://docs.gitlab.com/ee/user/markdown.html
* https://about.gitlab.com/handbook/markdown-guide/
* https://about.gitlab.com/blog/2018/08/17/gitlab-markdown-tutorial/
