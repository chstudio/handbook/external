= link:https://www.openssh.com/[SSH]
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2022-10
:lang: fr-FR

== Resources

* https://fr.wikipedia.org/wiki/Secure_Shell
* https://www.openssh.com/
* https://www.hostinger.fr/tutoriels/generer-cle-ssh
* https://security.stackexchange.com/questions/50878/ecdsa-vs-ecdh-vs-ed25519-vs-curve25519
