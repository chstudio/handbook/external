= link:https://docs.docker.com[Docker]
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2022-10
:lang: fr-FR

== Resources

* https://docs.docker.com/engine/install/linux-postinstall/
* https://docs.docker.com/engine/install/ubuntu/
* https://docs.docker.com/desktop/install/ubuntu/
