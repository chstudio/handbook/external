= link:https://gitlab.com/chstudio[Gitlab]
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2021-12
:lang: fr-FR

## Qu'est-ce que c'est ?

Une application web nous permettant de gérer nos développements. Elle intègre la
gestion du code source (dépôts xref:git.adoc[Git]), l'intégration continue (contrôle
qualité, tests, build) et contient aussi la documentation à travers les tickets,
milestone, wiki et autres…

## Pourquoi pas GitHub ?

Effectivement Gitlab et GitHub sont deux plateformes concurrentes. Elles nous
permetteraient de répondre à nos besoins. Une des plus grandes forces de Gitlab
est de nous permettre à travers un seul outil d'avoir accès à tout ce dont nous
avons besoin.

La souplesse de la solution nous a aujourd'hui prouvée qu'elle peut s'adapter
aux différents types de projet gérés en interne.

## Comment ?

Une organisation spécifique a été créé pour CH Studio. Cette organisation
contient tout les projets de l'entreprise.

Nous utilisons ensuite des sous groupes pour nous permettre de "ranger" les
projets correctement :

* link:https://gitlab.com/chstudio/clients[Clients] pour les projets clients
  bien sur.
** Chaque client a son propre sous groupe qui contiendra ses projets.
* Pour les projets plus conséquent, un groupe dédié permet d'avoir accès à la
licence Gitlab: https://gitlab.com/chstudio-lab/

Nous donnons parfois accès aux clients à leurs groupes ou projet. Tout dépend
des besoins et des compétences de l'équipe. Les issues sont aussi utilisées
pour échanger avec le client, il est impliqué dans leur création et leur
gestion pour nous permettre d'avoir un unique point d'entrée pour gérer les
bugs et évolutions.

Tout ce qui est créé sur Gitlab est bien sur lié à la technique, il arrive donc
d'utiliser d'autres outils pour échanger avec toutes les personnes impliquées
dans le project quand il est trop compliqué à utiliser.

Cependant toute discussion technique est créé et centralisée sur Gitlab. Même
lorsque des discussions démarre dans un outil tiers, un ticket est créé pour
discuter des points techniques et nous faisons le lien avec l'outil.

## Accès aux repository

Il est fortement conseillé d'utiliser SSH pour se connecter sur les repository.
Si vous n'avez pas encore généré de clé SSH, voici les commandes vous permettant
de le faire:

```bash
# Generation de la clé
ssh-keygen -t ed25519 -C "<comment>"

# Copie de la clé dans le presse papier
xclip -sel clip < ~/.ssh/id_ed25519.pub

# Il faut ensuite aller ajouter la clé dans votre profil : https://gitlab.com/-/profile/keys

# Test de connexion !
ssh -T git@gitlab.com
```

Référence vers la documentation Gitlab:

* https://docs.gitlab.com/ee/user/ssh.html
